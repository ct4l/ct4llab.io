===================
Messaging
===================

Collection of classes representing messages that are
exchanged among *ct4l* entities.

For example, warnings and errors in a parsing procedure
are communicated to the caller via message objects. 
Also, any implementation of the *Observer* pattern 
or any other notification mechanism must exchange 
message objects.

Each kind of message extends the class :cpp:class:`ct4l::Message`,
which allows for the following data to be stored in its instances:

- *code*: an element of :cpp:enum:`ct4l::MessageCode`, indicating a category to which the message belongs
- *type*: the type of the message, also indicating its priority, being an element of :cpp:enum:`ct4l::Message::Level`
- *short_text*: a short description (or the subject) of the message
- *details*: an optional detailed description

Message with column/row location
--------------------------------

The class :cpp:class:`ct4l::RowColMessage` represents messages 
containing an indication of the row and column (whose meanings are given by the context)
they refer to. For example, when parsing a formula, a parser may produce 
messages with the location of the exact row and column in which a parsing error or warning has occurred.

API
---

.. doxygenclass:: ct4l::Message
    :members:

.. doxygenenum:: ct4l::MessageCode

.. doxygenclass:: ct4l::RowColMessage
    :members: