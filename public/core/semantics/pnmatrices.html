
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PN-matrices &#8212; ct4l-lib  documentation</title>
    <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../../_static/alabaster.css" type="text/css" />
    <script id="documentation_options" data-url_root="../../" src="../../_static/documentation_options.js"></script>
    <script src="../../_static/jquery.js"></script>
    <script src="../../_static/underscore.js"></script>
    <script src="../../_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Proof-theory" href="../proof-theory/index.html" />
    <link rel="prev" title="Multi-algebras" href="multi_algebras.html" />
   
  <link rel="stylesheet" href="../../_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="pn-matrices">
<h1>PN-matrices<a class="headerlink" href="#pn-matrices" title="Permalink to this headline">¶</a></h1>
<p>Given a propositional signature <span class="math notranslate nohighlight">\(\Sigma\)</span>,
a PN-matrix over <span class="math notranslate nohighlight">\(\Sigma\)</span> is a structure
<span class="math notranslate nohighlight">\(\mathbb{M} = \langle \mathbf{M}, \{D_i\}_{i \in I} \rangle\)</span>,
where <span class="math notranslate nohighlight">\(\mathbf{M}\)</span> is a multi-algebra over <span class="math notranslate nohighlight">\(\Sigma\)</span>
and <span class="math notranslate nohighlight">\(D_i \subseteq M\)</span> is called a <em>distinguished set</em>, for each <span class="math notranslate nohighlight">\(i \in I\)</span>.</p>
<p>In <em>ct4l</em>, we represent finite PN-matrices using the class
<a class="reference internal" href="#_CPPv4I0EN4ct4l8PNMatrixE" title="ct4l::PNMatrix"><code class="xref cpp cpp-class docutils literal notranslate"><span class="pre">ct4l::PNMatrix</span></code></a>, which holds a pointer to
a <a class="reference internal" href="multi_algebras.html#_CPPv4I0EN4ct4l12MultiAlgebraE" title="ct4l::MultiAlgebra"><code class="xref cpp cpp-class docutils literal notranslate"><span class="pre">ct4l::MultiAlgebra</span></code></a> object and
a vector of subsets of the carrier of the multi-algebra.
We also store internally the translation of such subsets to
the domain values.</p>
<p>When building a PN-matrix, you may specify whether you want the
constructor to compute the complements of each distinguished set
and store it as a distinguished set too. For example, if the carrier of the algebra is <span class="math notranslate nohighlight">\(\{0,1\}\)</span> and you
opt for this, then passing a vector having only the set <span class="math notranslate nohighlight">\(\{1\}\)</span>
will produce a PN-matrix with <span class="math notranslate nohighlight">\(D_1 = \{1\}\)</span> and
<span class="math notranslate nohighlight">\(D_2 = \{0\}\)</span>. If the carrier of the algebra is <span class="math notranslate nohighlight">\(\{0, 1, 2, 3\}\)</span> and you give to the constructor a vector
with sets <span class="math notranslate nohighlight">\(\{0, 1\}\)</span> and <span class="math notranslate nohighlight">\(\{0\}\)</span>, then, internally,
this vector will have four sets, in this sequence:
<span class="math notranslate nohighlight">\(\{0, 1\}, \{2, 3\}, \{0\}, \{1, 2, 3\}\)</span>. This is intended to help
when we work with some notions of entailment associated to a PN-matrix.
Check <a class="reference internal" href="#_CPPv4N4ct4l8PNMatrix8PNMatrixEDT8_algebraERKNSt6vectorINSt3setI1TEEEEb" title="ct4l::PNMatrix::PNMatrix"><code class="xref cpp cpp-func docutils literal notranslate"><span class="pre">ct4l::PNMatrix::PNMatrix()</span></code></a> to see how to disable this option.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>Notice that the notion of PN-matrix we are using is a generalization
of the usual one, which accepts only a single designated set. By taking
a family, as we implemented, we can work more easily with many different notions
of entailment, including two-dimensional (and <span class="math notranslate nohighlight">\(n\)</span>-dimensional) notions.</p>
</div>
<div class="section" id="example-classical-logic-matrix">
<h2>Example: Classical Logic matrix<a class="headerlink" href="#example-classical-logic-matrix" title="Permalink to this headline">¶</a></h2>
<p>Take the boolean algebra built in <a class="reference internal" href="multi_algebras.html#boolean-algebra-example"><span class="std std-ref">Example: Building the two-element Boolean algebra</span></a>
and assume we have a pointer to it stored in the variable <cite>algebra</cite>.
The code below creates a PN-matrix based on such algebra:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">ct4l</span><span class="p">::</span><span class="n">PNMatrix</span> <span class="n">matrix</span> <span class="p">{</span><span class="n">algebra</span><span class="p">,</span> <span class="p">{{</span><span class="s2">&quot;T&quot;</span><span class="p">}}};</span>
</pre></div>
</div>
</div>
<div class="section" id="api">
<h2>API<a class="headerlink" href="#api" title="Permalink to this headline">¶</a></h2>
<dl class="cpp class">
<dt id="_CPPv4I0EN4ct4l8PNMatrixE">
<span id="_CPPv3I0EN4ct4l8PNMatrixE"></span><span id="_CPPv2I0EN4ct4l8PNMatrixE"></span><span class="pre">template&lt;</span><span class="pre">typename</span> <code class="sig-name descname"><span class="pre">T</span></code><span class="pre">&gt;</span><br /><span class="target" id="classct4l_1_1_p_n_matrix"></span><em class="property"><span class="pre">class</span> </em><code class="sig-prename descclassname"><span class="pre">ct4l</span><code class="sig-prename descclassname"><span class="pre">::</span></code></code><code class="sig-name descname"><span class="pre">PNMatrix</span></code><a class="headerlink" href="#_CPPv4I0EN4ct4l8PNMatrixE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Represents a non-deterministic matrix, allowing for empty sets in the interpretations, as well as multiple distinguished sets. </p>
<p><dl class="simple">
<dt><strong>Author</strong></dt><dd><p>Vitor Greati </p>
</dd>
</dl>
</p>
<div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric" id="breathe-section-title-public-functions">Public Functions</p>
<dl class="cpp function">
<dt id="_CPPv4N4ct4l8PNMatrix8PNMatrixEDT8_algebraERKNSt6vectorINSt3setI1TEEEEb">
<span id="_CPPv3N4ct4l8PNMatrix8PNMatrixEDT8_algebraERKNSt6vectorINSt3setI1TEEEEb"></span><span id="_CPPv2N4ct4l8PNMatrix8PNMatrixEDT8_algebraERKNSt6vectorINSt3setI1TEEEEb"></span><span class="target" id="classct4l_1_1_p_n_matrix_1ad89d652eb459745d4853bb3933592e42"></span><code class="sig-name descname"><span class="pre">PNMatrix</span></code><span class="sig-paren">(</span><span class="pre">decltype(</span><span class="pre">_algebra</span><span class="pre">)</span> <em><span class="pre">algebra</span></em>, <em class="property"><span class="pre">const</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">vector</span><span class="pre">&lt;</span><span class="pre">std</span><span class="pre">::</span><span class="pre">set</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l8PNMatrixE" title="ct4l::PNMatrix::T"><span class="pre">T</span></a><span class="pre">&gt;</span><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">dsets_domain</span></em>, <span class="pre">bool</span> <em><span class="pre">make_complements</span></em> <span class="pre">=</span> <span class="pre">true</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv4N4ct4l8PNMatrix8PNMatrixEDT8_algebraERKNSt6vectorINSt3setI1TEEEEb" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Constructor that accepts the algebra, the dsets and a flat indicating whether the complement of each dset should be added as a dset. </p>
<p>In case the flag is true, for each i, dset[2i]=D_i and dset[2i+1]=V\D_i.</p>
<p><dl class="simple">
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">algebra</span></code>: the pointer to the algebra </p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">dsets</span></code>: the designated sets in terms of the domain values </p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">make_complements</span></code>: indicates whether the complements of dsets should be computed </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l8PNMatrix5dsetsEv">
<span id="_CPPv3NK4ct4l8PNMatrix5dsetsEv"></span><span id="_CPPv2NK4ct4l8PNMatrix5dsetsEv"></span><span id="ct4l::PNMatrix::dsetsC"></span><span class="target" id="classct4l_1_1_p_n_matrix_1a63e9a315acce30a06e4ef80979cf9136"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">decltype(</span><span class="pre">_dsets</span><span class="pre">)</span> <code class="sig-name descname"><span class="pre">dsets</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l8PNMatrix5dsetsEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>The distinguished sets in terms of the internal representation. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>distinguished sets in internal representation </p>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l8PNMatrix12dsets_domainEv">
<span id="_CPPv3NK4ct4l8PNMatrix12dsets_domainEv"></span><span id="_CPPv2NK4ct4l8PNMatrix12dsets_domainEv"></span><span id="ct4l::PNMatrix::dsets_domainC"></span><span class="target" id="classct4l_1_1_p_n_matrix_1a53065243a8bc39f9d12fcbdab76095d4"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">decltype(</span><span class="pre">_dsets_domain</span><span class="pre">)</span> <code class="sig-name descname"><span class="pre">dsets_domain</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l8PNMatrix12dsets_domainEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>The distinguished sets in terms of the domain representation. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>distinguished sets in domain representation </p>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l8PNMatrix7algebraEv">
<span id="_CPPv3NK4ct4l8PNMatrix7algebraEv"></span><span id="_CPPv2NK4ct4l8PNMatrix7algebraEv"></span><span id="ct4l::PNMatrix::algebraC"></span><span class="target" id="classct4l_1_1_p_n_matrix_1af784d7e9aae6df486a9c261944848857"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">decltype(</span><span class="pre">_algebra</span><span class="pre">)</span> <code class="sig-name descname"><span class="pre">algebra</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l8PNMatrix7algebraEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Return a pointer to the matrix algebra. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>a pointer to the algebra </p>
</dd>
</dl>
</p>
</dd></dl>

</div>
</dd></dl>

</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="../../index.html">ct4l-lib</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../../usage/installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../usage/quickstart.html">Quickstart</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="../index.html">Core</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="../syntax/index.html">Syntax</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="index.html">Semantics</a></li>
<li class="toctree-l2"><a class="reference internal" href="../proof-theory/index.html">Proof-theory</a></li>
</ul>
</li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="../../index.html">Documentation overview</a><ul>
  <li><a href="../index.html">Core</a><ul>
  <li><a href="index.html">Semantics</a><ul>
      <li>Previous: <a href="multi_algebras.html" title="previous chapter">Multi-algebras</a></li>
      <li>Next: <a href="../proof-theory/index.html" title="next chapter">Proof-theory</a></li>
  </ul></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2021, VGreati, PFilipe, SMarcelino, CCaleiro, JMarcos.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.5.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="../../_sources/core/semantics/pnmatrices.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>