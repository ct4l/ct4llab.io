
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Domains &#8212; ct4l-lib  documentation</title>
    <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../../_static/alabaster.css" type="text/css" />
    <script id="documentation_options" data-url_root="../../" src="../../_static/documentation_options.js"></script>
    <script src="../../_static/jquery.js"></script>
    <script src="../../_static/underscore.js"></script>
    <script src="../../_static/doctools.js"></script>
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Truth-tables" href="truth_tables.html" />
    <link rel="prev" title="Semantics" href="index.html" />
   
  <link rel="stylesheet" href="../../_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="domains">
<h1>Domains<a class="headerlink" href="#domains" title="Permalink to this headline">¶</a></h1>
<p>Domains are intented to allow for the interpretation
and manipulation of values of different types (text, integers, tuples and so on)
using an internal representation based on integers.
In short, a domain provides ways to translate what we call
<em>domain values</em> into <em>internal values</em> and vice-versa.
With domains, we will be able to create algebras (and, consequently, matrices)
capable of dealing with values other than integers, while
keeping the internal operations only over integers.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>The data types to be used in a domain need at least to
provide a weak strict ordering implementation (in the <code class="xref cpp cpp-func docutils literal notranslate"><span class="pre">operator&lt;()</span></code> method), as internally
we use the domain values as keys in a map.</p>
</div>
<p>For example, below we create a domain over strings:</p>
<div class="highlight-cpp notranslate"><div class="highlight"><pre><span></span><span class="n">ct4l</span><span class="o">::</span><span class="n">Domain</span><span class="o">&lt;</span><span class="n">std</span><span class="o">::</span><span class="n">string</span><span class="o">&gt;</span> <span class="n">domain</span> <span class="p">{</span><span class="s">&quot;T&quot;</span><span class="p">,</span> <span class="s">&quot;I&quot;</span><span class="p">,</span> <span class="s">&quot;F&quot;</span><span class="p">};</span>
</pre></div>
</div>
<p>What happens internally is that the value <cite>“T”</cite> is mapped
into the value <cite>0</cite>, <cite>“I”</cite> to the value 1 and <cite>“F”</cite> to the value <cite>2</cite>.
We can then use the methods <a class="reference internal" href="#_CPPv4NK4ct4l6Domain9to_domainERKi" title="ct4l::Domain::to_domain"><code class="xref cpp cpp-func docutils literal notranslate"><span class="pre">ct4l::Domain::to_domain()</span></code></a>
and <a class="reference internal" href="#_CPPv4NK4ct4l6Domain11to_internalERK1T" title="ct4l::Domain::to_internal"><code class="xref cpp cpp-func docutils literal notranslate"><span class="pre">ct4l::Domain::to_internal()</span></code></a>
in order to translate values from one representation into the other:</p>
<div class="highlight-cpp notranslate"><div class="highlight"><pre><span></span><span class="n">std</span><span class="o">::</span><span class="n">string</span> <span class="n">internal_value_of_T</span> <span class="o">=</span> <span class="n">domain</span><span class="p">.</span><span class="n">to_internal</span><span class="p">(</span><span class="s">&quot;T&quot;</span><span class="p">);</span> <span class="c1">// the value 0</span>
<span class="kt">int</span> <span class="n">domain_value_of_1</span> <span class="o">=</span> <span class="n">domain</span><span class="p">.</span><span class="n">to_domain</span><span class="p">(</span><span class="mi">1</span><span class="p">);</span> <span class="c1">// the value &quot;I&quot;</span>
</pre></div>
</div>
<p>These methods are also implemented over containers <cite>vector</cite> and <cite>set</cite>
in the natural way.</p>
<p>Below we offer another example, in which the domain values are pairs of integers,
something common when dealing with product algebras or matrices:</p>
<div class="highlight-cpp notranslate"><div class="highlight"><pre><span></span><span class="k">using</span> <span class="n">IntPair</span> <span class="o">=</span> <span class="n">std</span><span class="o">::</span><span class="n">pair</span><span class="o">&lt;</span><span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="o">&gt;</span><span class="p">;</span>

<span class="n">ct4l</span><span class="o">::</span><span class="n">Domain</span><span class="o">&lt;</span><span class="n">IntPair</span><span class="o">&gt;</span> <span class="n">prod_domain</span> <span class="o">=</span> <span class="p">{</span>
    <span class="p">{</span><span class="mi">0</span><span class="p">,</span> <span class="mi">0</span><span class="p">},</span>
    <span class="p">{</span><span class="mi">0</span><span class="p">,</span> <span class="mi">1</span><span class="p">},</span>
    <span class="p">{</span><span class="mi">1</span><span class="p">,</span> <span class="mi">0</span><span class="p">},</span>
    <span class="p">{</span><span class="mi">1</span><span class="p">,</span> <span class="mi">1</span><span class="p">}</span>
<span class="p">};</span>
</pre></div>
</div>
<div class="section" id="api">
<h2>API<a class="headerlink" href="#api" title="Permalink to this headline">¶</a></h2>
<dl class="cpp class">
<dt id="_CPPv4I0EN4ct4l6DomainE">
<span id="_CPPv3I0EN4ct4l6DomainE"></span><span id="_CPPv2I0EN4ct4l6DomainE"></span><span class="pre">template&lt;</span><span class="pre">typename</span> <code class="sig-name descname"><span class="pre">T</span></code><span class="pre">&gt;</span><br /><span class="target" id="classct4l_1_1_domain"></span><em class="property"><span class="pre">class</span> </em><code class="sig-prename descclassname"><span class="pre">ct4l</span><code class="sig-prename descclassname"><span class="pre">::</span></code></code><code class="sig-name descname"><span class="pre">Domain</span></code><a class="headerlink" href="#_CPPv4I0EN4ct4l6DomainE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Represents a domain, internally represented as integers. </p>
<p><dl class="simple">
<dt><strong>Author</strong></dt><dd><p>Vitor Greati </p>
</dd>
</dl>
</p>
<div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric" id="breathe-section-title-public-functions">Public Functions</p>
<dl class="cpp function">
<dt id="_CPPv4N4ct4l6Domain6DomainERKDT7_valuesE">
<span id="_CPPv3N4ct4l6Domain6DomainERKDT7_valuesE"></span><span id="_CPPv2N4ct4l6Domain6DomainERKDT7_valuesE"></span><span class="target" id="classct4l_1_1_domain_1a1577c52c4e9161f3cd9ead498ac53644"></span><code class="sig-name descname"><span class="pre">Domain</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">decltype(</span><span class="pre">_values</span><span class="pre">)</span> <span class="pre">&amp;</span><em><span class="pre">values</span></em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv4N4ct4l6Domain6DomainERKDT7_valuesE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Construct a domain from a vector of values. </p>
<p><dl class="simple">
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">values</span></code>: a vector of values </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4N4ct4l6Domain6DomainERKNSt16initializer_listI1TEE">
<span id="_CPPv3N4ct4l6Domain6DomainERKNSt16initializer_listI1TEE"></span><span id="_CPPv2N4ct4l6Domain6DomainERKNSt16initializer_listI1TEE"></span><span id="ct4l::Domain::Domain__std::initializer_list:T:CR"></span><span class="target" id="classct4l_1_1_domain_1ad7ca9c6ac06d02a61b7e7236369cfa92"></span><code class="sig-name descname"><span class="pre">Domain</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">initializer_list</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">values</span></em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv4N4ct4l6Domain6DomainERKNSt16initializer_listI1TEE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Construct a domain from an initializer list. </p>
<p><dl class="simple">
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">values</span></code>: a list of values </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain6valuesEv">
<span id="_CPPv3NK4ct4l6Domain6valuesEv"></span><span id="_CPPv2NK4ct4l6Domain6valuesEv"></span><span id="ct4l::Domain::valuesC"></span><span class="target" id="classct4l_1_1_domain_1a76534ce5ec724af06becbb3239425173"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">decltype(</span><span class="pre">_values</span><span class="pre">)</span> <code class="sig-name descname"><span class="pre">values</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain6valuesEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>The domain values. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the domain values as given in the construction of the domain </p>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain10values_setEv">
<span id="_CPPv3NK4ct4l6Domain10values_setEv"></span><span id="_CPPv2NK4ct4l6Domain10values_setEv"></span><span id="ct4l::Domain::values_setC"></span><span class="target" id="classct4l_1_1_domain_1ade8f0b56122794651055f74ebf9e8255"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">set</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <code class="sig-name descname"><span class="pre">values_set</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain10values_setEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Produce the values as a set. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the set of domain values </p>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain11translationEv">
<span id="_CPPv3NK4ct4l6Domain11translationEv"></span><span id="_CPPv2NK4ct4l6Domain11translationEv"></span><span id="ct4l::Domain::translationC"></span><span class="target" id="classct4l_1_1_domain_1abc60a4c3be0e3acfe1d43ae60c8ff7c7"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">decltype(</span><span class="pre">_translation</span><span class="pre">)</span> <code class="sig-name descname"><span class="pre">translation</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain11translationEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Truth-table translation of domain values into integers. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the truth-table translation from domain values into integers </p>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain4sizeEv">
<span id="_CPPv3NK4ct4l6Domain4sizeEv"></span><span id="_CPPv2NK4ct4l6Domain4sizeEv"></span><span id="ct4l::Domain::sizeC"></span><span class="target" id="classct4l_1_1_domain_1ad034512a9433a41e9ab85c034198f1ba"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">auto</span> <code class="sig-name descname"><span class="pre">size</span></code><span class="sig-paren">(</span><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain4sizeEv" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>The domain size. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the domain size </p>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain11to_internalERK1T">
<span id="_CPPv3NK4ct4l6Domain11to_internalERK1T"></span><span id="_CPPv2NK4ct4l6Domain11to_internalERK1T"></span><span id="ct4l::Domain::to_internal__TCRC"></span><span class="target" id="classct4l_1_1_domain_1af2b94e6dec0d075a348688cfb5fb39e4"></span><span class="pre">int</span> <code class="sig-name descname"><span class="pre">to_internal</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a> <span class="pre">&amp;</span><em><span class="pre">v</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain11to_internalERK1T" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Translate a domain value into the corresponding internal value. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the corresponding internal value </p>
</dd>
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">v</span></code>: the domain value to be translated </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain11to_internalERKNSt3setI1TEE">
<span id="_CPPv3NK4ct4l6Domain11to_internalERKNSt3setI1TEE"></span><span id="_CPPv2NK4ct4l6Domain11to_internalERKNSt3setI1TEE"></span><span id="ct4l::Domain::to_internal__std::set:T:CRC"></span><span class="target" id="classct4l_1_1_domain_1ae9e778fe0c1dd74cfa2f66043327fa32"></span><span class="pre">std</span><span class="pre">::</span><span class="pre">set</span><span class="pre">&lt;</span><span class="pre">int</span><span class="pre">&gt;</span> <code class="sig-name descname"><span class="pre">to_internal</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">set</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">vs</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain11to_internalERKNSt3setI1TEE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Apply <a class="reference internal" href="#classct4l_1_1_domain_1af2b94e6dec0d075a348688cfb5fb39e4"><span class="std std-ref">to_internal(const T&amp;) const </span></a> component-wise in a set. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the corresponding translation into internal values </p>
</dd>
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">vs</span></code>: the values to be translated </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain11to_internalERKNSt6vectorI1TEE">
<span id="_CPPv3NK4ct4l6Domain11to_internalERKNSt6vectorI1TEE"></span><span id="_CPPv2NK4ct4l6Domain11to_internalERKNSt6vectorI1TEE"></span><span id="ct4l::Domain::to_internal__std::vector:T:CRC"></span><span class="target" id="classct4l_1_1_domain_1a044c9be33e642e49c4d643bfbbdd098b"></span><span class="pre">std</span><span class="pre">::</span><span class="pre">vector</span><span class="pre">&lt;</span><span class="pre">int</span><span class="pre">&gt;</span> <code class="sig-name descname"><span class="pre">to_internal</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">vector</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">vs</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain11to_internalERKNSt6vectorI1TEE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Apply <a class="reference internal" href="#classct4l_1_1_domain_1af2b94e6dec0d075a348688cfb5fb39e4"><span class="std std-ref">to_internal(const T&amp;) const </span></a> component-wise in a vector. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the corresponding translation into internal values </p>
</dd>
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">vs</span></code>: the values to be translated </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain9to_domainERKi">
<span id="_CPPv3NK4ct4l6Domain9to_domainERKi"></span><span id="_CPPv2NK4ct4l6Domain9to_domainERKi"></span><span id="ct4l::Domain::to_domain__iCRC"></span><span class="target" id="classct4l_1_1_domain_1a155fdf261a4c9d3cd6b31d7cee7d52e6"></span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a> <code class="sig-name descname"><span class="pre">to_domain</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">int</span> <span class="pre">&amp;</span><em><span class="pre">v</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain9to_domainERKi" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Translate an internal value into the corresponding domain value. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the corresponding domain value </p>
</dd>
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">v</span></code>: the internal value to be translated </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain9to_domainERKNSt3setIiEE">
<span id="_CPPv3NK4ct4l6Domain9to_domainERKNSt3setIiEE"></span><span id="_CPPv2NK4ct4l6Domain9to_domainERKNSt3setIiEE"></span><span id="ct4l::Domain::to_domain__std::set:i:CRC"></span><span class="target" id="classct4l_1_1_domain_1a74f627ce6f474bf8a2a55b5d7282f884"></span><span class="pre">std</span><span class="pre">::</span><span class="pre">set</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <code class="sig-name descname"><span class="pre">to_domain</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">set</span><span class="pre">&lt;</span><span class="pre">int</span><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">vs</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain9to_domainERKNSt3setIiEE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Apply <a class="reference internal" href="#classct4l_1_1_domain_1a155fdf261a4c9d3cd6b31d7cee7d52e6"><span class="std std-ref">to_domain(const int&amp;) const </span></a> component-wise in a set. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the corresponding translation </p>
</dd>
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">vs</span></code>: the internal values to be translated </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6Domain9to_domainERKNSt6vectorIiEE">
<span id="_CPPv3NK4ct4l6Domain9to_domainERKNSt6vectorIiEE"></span><span id="_CPPv2NK4ct4l6Domain9to_domainERKNSt6vectorIiEE"></span><span id="ct4l::Domain::to_domain__std::vector:i:CRC"></span><span class="target" id="classct4l_1_1_domain_1ada597004c309d3a8dfe0b68ea882f8d7"></span><span class="pre">std</span><span class="pre">::</span><span class="pre">vector</span><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <code class="sig-name descname"><span class="pre">to_domain</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <span class="pre">std</span><span class="pre">::</span><span class="pre">vector</span><span class="pre">&lt;</span><span class="pre">int</span><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">vs</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6Domain9to_domainERKNSt6vectorIiEE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Apply <a class="reference internal" href="#classct4l_1_1_domain_1a155fdf261a4c9d3cd6b31d7cee7d52e6"><span class="std std-ref">to_domain(const int&amp;) const </span></a> component-wise in a vector. </p>
<p><dl class="simple">
<dt><strong>Return</strong></dt><dd><p>the corresponding translation </p>
</dd>
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">vs</span></code>: the internal values to be translated </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6DomaineqERK6DomainI1TE">
<span id="_CPPv3NK4ct4l6DomaineqERK6DomainI1TE"></span><span id="_CPPv2NK4ct4l6DomaineqERK6DomainI1TE"></span><span id="ct4l::Domain::eq-operator__Domain:T:CRC"></span><span class="target" id="classct4l_1_1_domain_1ad8c3d7d85ee714d652393282964a1d13"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">bool</span> <code class="sig-name descname"><span class="pre">operator==</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain"><span class="pre">Domain</span></a><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">other</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6DomaineqERK6DomainI1TE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Equality of domains. </p>
<p><dl class="simple">
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">other</span></code>: the other domain </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

<dl class="cpp function">
<dt id="_CPPv4NK4ct4l6DomainneERK6DomainI1TE">
<span id="_CPPv3NK4ct4l6DomainneERK6DomainI1TE"></span><span id="_CPPv2NK4ct4l6DomainneERK6DomainI1TE"></span><span id="ct4l::Domain::neq-operator__Domain:T:CRC"></span><span class="target" id="classct4l_1_1_domain_1a59dadc39d1350d99ad92529b337e018e"></span><em class="property"><span class="pre">inline</span></em> <span class="pre">bool</span> <code class="sig-name descname"><span class="pre">operator!=</span></code><span class="sig-paren">(</span><em class="property"><span class="pre">const</span></em> <a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain"><span class="pre">Domain</span></a><span class="pre">&lt;</span><a class="reference internal" href="#_CPPv4I0EN4ct4l6DomainE" title="ct4l::Domain::T"><span class="pre">T</span></a><span class="pre">&gt;</span> <span class="pre">&amp;</span><em><span class="pre">other</span></em><span class="sig-paren">)</span> <em class="property"><span class="pre">const</span></em><a class="headerlink" href="#_CPPv4NK4ct4l6DomainneERK6DomainI1TE" title="Permalink to this definition">¶</a><br /></dt>
<dd><p>Non-equality of domains. </p>
<p><dl class="simple">
<dt><strong>Parameters</strong></dt><dd><ul class="breatheparameterlist simple">
<li><p><code class="docutils literal notranslate"><span class="pre">other</span></code>: the other domain </p></li>
</ul>
</dd>
</dl>
</p>
</dd></dl>

</div>
</dd></dl>

</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="../../index.html">ct4l-lib</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../../usage/installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../usage/quickstart.html">Quickstart</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="../index.html">Core</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="../syntax/index.html">Syntax</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="index.html">Semantics</a></li>
<li class="toctree-l2"><a class="reference internal" href="../proof-theory/index.html">Proof-theory</a></li>
</ul>
</li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="../../index.html">Documentation overview</a><ul>
  <li><a href="../index.html">Core</a><ul>
  <li><a href="index.html">Semantics</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Semantics</a></li>
      <li>Next: <a href="truth_tables.html" title="next chapter">Truth-tables</a></li>
  </ul></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2021, VGreati, PFilipe, SMarcelino, CCaleiro, JMarcos.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.5.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="../../_sources/core/semantics/domains.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>