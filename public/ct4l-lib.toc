\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Installation}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Pre\sphinxhyphen {}requisites}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Building}{3}{section.1.2}%
\contentsline {chapter}{\numberline {2}Quickstart}{5}{chapter.2}%
\contentsline {chapter}{\numberline {3}Core}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}Syntax}{7}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Signatures}{7}{subsection.3.1.1}%
\contentsline {subsubsection}{Propositional signatures}{7}{subsubsection*.3}%
\contentsline {subsubsection}{API}{8}{subsubsection*.4}%
\contentsline {section}{\numberline {3.2}Semantics}{13}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Domains}{13}{subsection.3.2.1}%
\contentsline {subsubsection}{API}{13}{subsubsection*.87}%
\contentsline {subsection}{\numberline {3.2.2}Truth\sphinxhyphen {}tables}{16}{subsection.3.2.2}%
\contentsline {subsubsection}{Constructing truth\sphinxhyphen {}tables}{16}{subsubsection*.132}%
\contentsline {subsubsection}{Manipulating truth\sphinxhyphen {}tables}{18}{subsubsection*.133}%
\contentsline {subsubsection}{Iterating over truth\sphinxhyphen {}tables}{18}{subsubsection*.134}%
\contentsline {subsubsection}{API}{19}{subsubsection*.135}%
\contentsline {subsection}{\numberline {3.2.3}Multi\sphinxhyphen {}algebras}{27}{subsection.3.2.3}%
\contentsline {subsubsection}{Example: Building the two\sphinxhyphen {}element Boolean algebra}{27}{subsubsection*.286}%
\contentsline {subsubsection}{Accessing the algebra operations}{28}{subsubsection*.287}%
\contentsline {subsubsection}{Adding interpretations}{28}{subsubsection*.288}%
\contentsline {subsubsection}{API}{28}{subsubsection*.289}%
\contentsline {subsection}{\numberline {3.2.4}PN\sphinxhyphen {}matrices}{30}{subsection.3.2.4}%
\contentsline {subsubsection}{Example: Classical Logic matrix}{30}{subsubsection*.319}%
\contentsline {subsubsection}{API}{30}{subsubsection*.320}%
\contentsline {section}{\numberline {3.3}Proof\sphinxhyphen {}theory}{31}{section.3.3}%
\contentsline {chapter}{Index}{33}{section*.335}%
